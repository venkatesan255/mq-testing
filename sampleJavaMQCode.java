import com.ibm.mq.*;
import com.ibm.mq.constants.CMQC;

public class MQScript {

    private static final String HOST = "MQ_SERVER_HOST";
    private static final int PORT = 1414;
    private static final String CHANNEL = "SYSTEM.DEF.SVRCONN";
    private static final String QUEUE_MANAGER = "QMGR_NAME";
    private static final String QUEUE_NAME_SEND = "SEND_QUEUE";
    private static final String QUEUE_NAME_RECEIVE = "RECEIVE_QUEUE";
    private static final String MESSAGE_TEXT = "Test message from LoadRunner";

    public static void main(String[] args) {
        MQQueueManager qMgr = null;
        MQQueue sendQueue = null;
        MQQueue receiveQueue = null;
        MQMessage message = null;
        MQMessage response = null;

        try {
            // Create connection details
            MQEnvironment.hostname = HOST;
            MQEnvironment.port = PORT;
            MQEnvironment.channel = CHANNEL;
            MQEnvironment.properties.put(CMQC.USER_ID_PROPERTY, ""); // Insert your user ID if needed
            MQEnvironment.properties.put(CMQC.PASSWORD_PROPERTY, ""); // Insert your password if needed

            // Connect to the queue manager
            qMgr = new MQQueueManager(QUEUE_MANAGER);

            // Open the send queue
            sendQueue = qMgr.accessQueue(QUEUE_NAME_SEND, MQC.MQOO_OUTPUT);

            // Open the receive queue
            receiveQueue = qMgr.accessQueue(QUEUE_NAME_RECEIVE, MQC.MQOO_INPUT_AS_Q_DEF);

            // Create a message
            message = new MQMessage();
            message.writeString(MESSAGE_TEXT);

            // Put the message on the send queue
            sendQueue.put(message);

            // Receive the response
            response = new MQMessage();
            receiveQueue.get(response);

            // Display the response
            System.out.println("Received response: " + response.readString());
        } catch (MQException mqe) {
            System.err.println("MQException: " + mqe.getMessage());
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        } finally {
            // Close queues and disconnect from queue manager
            try {
                if (sendQueue != null) {
                    sendQueue.close();
                }
                if (receiveQueue != null) {
                    receiveQueue.close();
                }
                if (qMgr != null) {
                    qMgr.disconnect();
                }
            } catch (MQException mqe) {
                System.err.println("MQException during cleanup: " + mqe.getMessage());
            }
        }
    }
}
